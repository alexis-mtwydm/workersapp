package com.example.workers.data.remote

import com.example.workers.data.model.Worker
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    @GET("workers")
    suspend fun getWorkers(): List<Worker>

    @POST("workers")
    suspend fun saveWorker(@Body worker: Worker?): Worker?
}
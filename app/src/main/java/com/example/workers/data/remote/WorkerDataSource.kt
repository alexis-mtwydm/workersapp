package com.example.workers.data.remote

import com.example.workers.data.model.Worker

class WorkerDataSource (private val apiService: ApiService){
    suspend fun getWorkers():List<Worker> = apiService.getWorkers()

    suspend fun saveWorker(worker:Worker?):Worker?{
        apiService.saveWorker(worker)

        return worker
    }
}
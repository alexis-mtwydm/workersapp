package com.example.workers.data.model

data class Worker (
    val id:String = "",
    val name:String = "",
    val position:String="",
    val location:String=""
    )
package com.example.workers.ui.workersdetails

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.workers.R
import com.example.workers.data.model.Worker
import com.example.workers.presentation.WorkerViewModel
import com.example.workers.ui.workers.WorkersListToolbar


@Composable
fun workerDetail(navigationController: NavHostController, workerViewModel: WorkerViewModel?, worker:Worker?){
    Scaffold(topBar = { WorkersListToolbar(navigationController, "Worker Details") }) {

        Card(shape = RoundedCornerShape(8.dp),
            modifier = Modifier
                .padding(6.dp)
                .fillMaxHeight(1f)){
            Column(modifier = Modifier
                .padding(25.dp)
                .fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
                Image(
                    painter = painterResource(id = R.drawable.user),
                    contentDescription = "Worker Image",
                    contentScale = ContentScale.Fit,
                    modifier = Modifier.width(200.dp).height(200.dp)
                )
                Row(modifier = Modifier.padding(top = 5.dp).fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center) {
                    Text("${worker?.name!!}", fontSize = 30.sp)
                }
                Row(modifier = Modifier.padding(top = 5.dp).fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center) {
                    Image(
                        painter = painterResource(id = R.drawable.job),
                        contentDescription = "Worker Position",
                        modifier = Modifier
                            .height(32.dp)
                            .width(32.dp),
                        contentScale = ContentScale.Fit
                    )
                    Text("${worker?.position!!}", fontSize = 24.sp, modifier = Modifier.padding(top = 3.dp, start = 3.dp))
                }
                Row(modifier = Modifier.padding(top = 5.dp).fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center) {
                    Image(
                        painter = painterResource(id = R.drawable.pin),
                        contentDescription = "Worker Pin",
                        modifier = Modifier
                            .height(32.dp)
                            .width(32.dp),
                        contentScale = ContentScale.Fit
                    )
                    Text("${worker?.location!!}", fontSize = 24.sp, modifier = Modifier.padding(top = 3.dp, start = 6.dp))
                }
            }
        }

    }
}

@Preview(showBackground = true)
@Composable
fun detailScreen(){
    Column(modifier = Modifier
        .padding(25.dp)
        .fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        Image(
            painter = painterResource(id = R.drawable.user),
            contentDescription = "Worker Image",
            contentScale = ContentScale.Fit
        )
        Row(modifier = Modifier.padding(top = 5.dp).fillMaxWidth(),
            horizontalArrangement = Arrangement.Center) {
            Text("Worker Name", fontSize = 25.sp)
        }
        Row(modifier = Modifier.padding(top = 5.dp).fillMaxWidth(),
            horizontalArrangement = Arrangement.Center) {
            Image(
                painter = painterResource(id = R.drawable.job),
                contentDescription = "Worker Position",
                modifier = Modifier
                    .height(25.dp)
                    .width(25.dp),
                contentScale = ContentScale.Fit
            )
            Text("Worker Position", fontSize = 16.sp, modifier = Modifier.padding(top = 3.dp, start = 3.dp))
        }
        Row(modifier = Modifier.padding(top = 5.dp).fillMaxWidth(),
            horizontalArrangement = Arrangement.Center) {
            Image(
                painter = painterResource(id = R.drawable.pin),
                contentDescription = "Worker Pin",
                modifier = Modifier
                    .height(25.dp)
                    .width(25.dp),
                contentScale = ContentScale.Fit
            )
            Text("Worker Location", fontSize = 16.sp, modifier = Modifier.padding(top = 3.dp, start = 6.dp))
        }
    }
}
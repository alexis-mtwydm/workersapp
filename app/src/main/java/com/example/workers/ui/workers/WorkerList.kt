package com.example.workers.ui.workers

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role.Companion.Image
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.workers.app.Routes
import com.example.workers.data.model.Worker
import com.example.workers.presentation.WorkerViewModel
import com.example.workers.ui.theme.WorkersTheme
import com.example.workers.R
import com.google.gson.Gson
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalLifecycleOwner
import com.example.workers.core.Resource
import java.util.*
import androidx.lifecycle.Observer
import com.example.workers.ui.helperComponents.customSpinner

@Composable
fun workerList(navigationController: NavHostController, workerViewModel: WorkerViewModel?){
    val owner = LocalLifecycleOwner.current
    var workers: List<Worker> by remember{ mutableStateOf(emptyList())}
    var showSpinner by remember { mutableStateOf(false) }
    workerViewModel?.fetchWorkers()!!.observe(owner, Observer{ result ->
        when(result){
            is Resource.Loading -> {
                showSpinner = true;
            }
            is Resource.Success -> {
                showSpinner = false ;
                workers = result.data
            }
            is Resource.Failure -> {
                showSpinner = false;

            }
        }
    })
    Scaffold(topBar = { WorkersListToolbar(navigationController, "Workers") },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                navigationController.navigate(Routes.WorkerAdd.route)
            }) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = "Add worker"
                )
            }
        },
        floatingActionButtonPosition = FabPosition.End
    ) {
        LazyColumn(){
            items(workers){ worker ->
                Card(shape = RoundedCornerShape(8.dp),
                    modifier = Modifier
                        .padding(6.dp)
                        .fillMaxWidth()
                        .clickable {
                            val workerToPass = Gson().toJson(worker)
                            navigationController.navigate(
                                Routes.WorkerDetail.route.replace(
                                    "{worker}",
                                    workerToPass
                                )
                            )
                        })
                {
                    Row(modifier = Modifier.padding(8.dp)){
                        Image(
                            painter = painterResource(id = R.drawable.user),
                            contentDescription = "Worker Image",
                            modifier = Modifier
                                .height(75.dp)
                                .width(75.dp),
                            contentScale = ContentScale.Fit
                        )
                        Column(modifier = Modifier.padding(top = 16.dp, start = 8.dp)) {
                            Text(text = "${worker.name}", fontSize = 30.sp)
                        }
                    }
                }
            }
        }
        if(showSpinner){
            customSpinner()

        }
    }
}

@Composable
fun workerList(
    navigationController: NavHostController,
    workers: List<Worker>){

}

@Preview(showBackground = true)
@Composable
fun listPreview(){
    WorkersTheme{
        workerList(
            navigationController = rememberNavController(),
            workers = listOf(
                Worker("1", "Alexis Lopez", "SE 2", "Remote"),
                Worker("2", "Oscar Lopez", "SE 3", "Remote"))
        )
    }
}



package com.example.workers.ui.workersdetails

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.Absolute.Center
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.workers.data.model.Worker
import com.example.workers.presentation.WorkerViewModel
import com.example.workers.ui.workers.WorkersListToolbar
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.Observer
import com.example.workers.core.Resource
import com.example.workers.ui.helperComponents.customDialog

@Composable
fun workerAdd(navigationController: NavHostController, workerViewModel: WorkerViewModel?){
    var workerName by remember{ mutableStateOf("")}
    var workerPosition by remember{ mutableStateOf("")}
    var workerLocation by remember{ mutableStateOf("")}
    var owner = LocalLifecycleOwner.current
    val context = LocalContext.current
    var showDialog by remember { mutableStateOf(false) }

    Scaffold(topBar = { WorkersListToolbar(navigationController,"New Worker") }) {
        Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
            OutlinedTextField(value = workerName, onValueChange ={t-> workerName = t}, label = { Text(text = "Worker Name")} ,modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(1f))
            OutlinedTextField(value = workerPosition, onValueChange ={t-> workerPosition = t}, label = { Text(text = "Worker Position")} ,modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(1f))
            OutlinedTextField(value = workerLocation, onValueChange ={t-> workerLocation = t}, label = { Text(text = "Worker Location")} ,modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(1f))
            Button(onClick =
            {
                val workerToAdd: Worker = Worker("0", workerName, workerPosition, workerLocation)
                workerViewModel?.saveWorker(workerToAdd)!!.observe(owner,Observer{ result ->
                    when(result){

                        is Resource.Loading -> {
                            showDialog = true
                        }
                        is Resource.Success -> {
                            showDialog = false
                            Toast.makeText(context, "Worker Added!",Toast.LENGTH_SHORT).show()
                            navigationController.popBackStack()
                        }
                        is Resource.Failure -> {
                            showDialog = false
                            Toast.makeText(context, "Error!",Toast.LENGTH_SHORT).show()
                            Log.d("Failure post", "Post didn't work")
                        }
                    }
                })
            }, modifier = Modifier
                .fillMaxWidth(0.7f)
                .padding(8.dp) ) {
                Text(text = "Add Worker")
            }
        }
        if(showDialog){
            customDialog()
        }
    }
}


package com.example.workers.ui.helperComponents

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties

@Composable
fun aboutDialog(
    showDialog: Boolean,
    setShowDialog: (Boolean) -> Unit
){
    Dialog(
        onDismissRequest = { setShowDialog(false) },
        DialogProperties(dismissOnBackPress = true, dismissOnClickOutside = true)
    ) {
        Box(
            contentAlignment= Alignment.Center,
            modifier = Modifier
                .size(width = 250.dp, height = 100.dp)
                .background(Color.White, shape = RoundedCornerShape(8.dp))
        ) {
            Column(modifier = Modifier.fillMaxSize().padding(8.dp),
            verticalArrangement = Arrangement.Center) {
                Row(){
                    Text("Autor: Alexis Lopez")
                }
                Row(){
                    Text("MTWYDM - Android 2022")
                }
                Row(){
                    Text("Correo: alexislml96@gmail.com")
                }
            }
        }
    }
}
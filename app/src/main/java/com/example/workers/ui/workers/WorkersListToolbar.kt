package com.example.workers.ui.workers

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import com.example.workers.ui.helperComponents.customDialog
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavHostController
import com.example.workers.ui.helperComponents.OverflowMenuAction
import com.example.workers.ui.helperComponents.aboutDialog

@Composable
fun WorkersListToolbar(navigationController: NavHostController, title:String){
    val (isExpanded, setExpanded) = remember { mutableStateOf(false) }
    val (showDialog, setShowDialog) = remember { mutableStateOf(false) }

    TopAppBar(title = { Text(title, color = Color.White) },
        backgroundColor = darkColors().primaryVariant,
        contentColor = Color.White,
        actions = {
            OverflowMenuAction(isExpanded, setExpanded, showDialog, setShowDialog)
        },
        navigationIcon = if (navigationController.previousBackStackEntry != null) {
            {
                IconButton(onClick = { navigationController.navigateUp() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Back"
                    )
                }
            }
        } else {
            {
                IconButton(onClick = { /*TODO*/ }) {
                    Icon(imageVector = Icons.Filled.Home, contentDescription = "home")
                }
            }
        }
    )
    if(showDialog){
        aboutDialog(showDialog, setShowDialog)
    }
}
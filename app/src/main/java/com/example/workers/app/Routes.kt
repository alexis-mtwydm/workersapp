package com.example.workers.app

sealed class Routes(val route:String) {
    object WorkerList:Routes("workerList")
    object WorkerAdd:Routes("workerAdd")
    object WorkerDetail:Routes("workerDetail/worker={worker}")
}
package com.example.workers

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHost
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.workers.app.Routes
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.workers.data.model.Worker
import com.example.workers.data.remote.ApiClient
import com.example.workers.data.remote.WorkerDataSource
import com.example.workers.presentation.WorkerViewModel
import com.example.workers.presentation.WorkerViewModelFactory
import com.example.workers.repository.WorkerRepositoryImp
import com.example.workers.ui.theme.WorkersTheme
import com.example.workers.ui.workers.workerList
import com.example.workers.ui.workersdetails.workerAdd
import com.example.workers.ui.workersdetails.workerDetail
import com.google.gson.Gson

class MainActivity : ComponentActivity() {

    private val viewModel by viewModels<WorkerViewModel> {
        WorkerViewModelFactory(WorkerRepositoryImp(
            WorkerDataSource(ApiClient.service)
        ))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WorkersTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var navigationController = rememberNavController()
                    NavHost(navigationController, startDestination = Routes.WorkerList.route){
                        composable(Routes.WorkerList.route){ workerList(navigationController, viewModel) }
                        composable(Routes.WorkerDetail.route) { backStackEntry ->
                            val workerJson = backStackEntry.arguments?.getString("worker")
                            val workerObject = Gson().fromJson(workerJson, Worker::class.java)
                            workerDetail(navigationController, viewModel, workerObject) }
                        composable(Routes.WorkerAdd.route){ workerAdd(navigationController, viewModel) }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}
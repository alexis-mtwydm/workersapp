package com.example.workers.repository

import com.example.workers.data.model.Worker
import com.example.workers.data.remote.WorkerDataSource

class WorkerRepositoryImp(private val dataSource: WorkerDataSource): WorkerRepository {
    override suspend fun getWorkers(): List<Worker> {
        return dataSource.getWorkers()
    }

    override suspend fun saveWorker(worker: Worker?): Worker? {
        return dataSource.saveWorker(worker)
    }
}
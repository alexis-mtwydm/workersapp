package com.example.workers.repository

import com.example.workers.data.model.Worker

interface WorkerRepository {
    suspend fun getWorkers():List<Worker>
    suspend fun saveWorker(worker:Worker?):Worker?
}
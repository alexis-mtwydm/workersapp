package com.example.workers.presentation

import android.util.Log
import android.util.Log.INFO
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.example.workers.core.Resource
import com.example.workers.data.model.Worker
import kotlinx.coroutines.Dispatchers
import com.example.workers.repository.WorkerRepository
import kotlinx.coroutines.launch
import java.lang.Exception

class WorkerViewModel(private val repository: WorkerRepository): ViewModel(){

    private val _workers = MutableLiveData<List<Worker>>()
    fun fetchWorkers() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getWorkers()))
        }
        catch(exception:Exception){
            emit(Resource.Failure(exception))
        }
    }
    fun saveWorker(worker:Worker?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.saveWorker(worker)))
        }
        catch(exception:Exception){
            emit(Resource.Failure(exception))
        }
    }
}

class WorkerViewModelFactory(private val repository: WorkerRepository): ViewModelProvider.Factory{
    override fun <T: ViewModel> create(modelClass:Class<T>):T{
        return modelClass.getConstructor(WorkerRepository::class.java).newInstance(repository)
    }
}